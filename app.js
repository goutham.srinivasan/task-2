//heading //

$(document).ready(function(){
    $("#btn-one").click(function() {
        $("#h1").show();
        $("#color-1").spectrum({
          preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            palette: [["red", "rgba(0, 255, 0, .5)", "rgb(255, 0, 255)"]],
            move:function(color){
              $('.header').css('background-color', color.toHexString());
            }
        });
      });
    $("#btn-one").click(function() {
        $("#h2").show();
        $("#color-2").spectrum({
          preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]],
            move:function(color){
              $('#header-text').css('color', color.toHexString());
              $('#header-text2').css('color', color.toHexString());
              $('#header-text3').css('color', color.toHexString());
              $('#header-text4').css('color', color.toHexString());
              $('#header-text5').css('color', color.toHexString());
            }
        });
      });
      $("#btn-one").click(function() {
        $("#h3").show();
        $("#color-3").spectrum({
           preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            palette: [["red", "rgba(0, 255, 0, .5)", "rgb(255, 0, 255)"]],
            move:function(color){
              $('#header-text').css('color', color.toHexString());
            }
        });
      });
      $("#btn-one").click(function() {
        $("#h4").show();
        $("#color-4").spectrum({
          preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 255, 255)"]],
            move:function(color){
              $('#header-text2').css('color', color.toHexString());
            }
        });
      });
      $("#btn-one").click(function() {
        $("#h5").show();
        $("#color-5").spectrum({
          preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]],
            move:function(color){
              $('.active-button').css('background-color', color.toHexString());
            }
        });
      });
      $("#btn-one").click(function() {
        $("#h6").show();
        $("#color-6").spectrum({
          preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]],
            move:function(color){
              $('#header-text3').css('color', color.toHexString());
            }
        });
      });
});

// sidebar //

$(document).ready(function(){
    $("#btn-two").click(function() {
        $("#s1").show();
        $("#color-one").spectrum({
          preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]],
            move:function(color){
              $('.get-touch').css('background-color', color.toHexString());
            }
        });
      });
    $("#btn-two").click(function() {
        $("#s2").show();
        $("#color-two").spectrum({
          preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]],
            move:function(color){
              $('.get-touch').css('color', color.toHexString());
            }
        });
      });
      $("#btn-two").click(function() {
        $("#s3").show();
        $("#color-three").spectrum({
          preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]],
            move:function(color){
              $('.visit-website').css('background-color', color.toHexString());
            }
        });
      });
      $("#btn-two").click(function() {
        $("#s4").show();
        $("#color-four").spectrum({
          preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]],
            move:function(color){
              $('.visit-website').css('background-color', color.toHexString());
            }
        });
      });
      $("#btn-two").click(function() {
        $("#s5").show();
        $("#color-five").spectrum({
          preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]],
            move:function(color){
              $('.phone-number').css('color', color.toHexString());
            }
        }).append( "<p>Test</p>" );
      });
      $("#btn-two").click(function() {
        $("#s6").show();
        $("#color-six").spectrum({
          preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]],
            move:function(color){
              $('.sidebar').css('background-color', color.toHexString());
            }
        });
      });
});

// main body // 

$(document).ready(function(){
    $("#btn-three").click(function() {
        $("#m1").show();
        $("#color-I").spectrum({
          preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]],
            move:function(color){
              $('.red-box').css('background-color', color.toHexString());
            }
        });
      });
    $("#btn-three").click(function() {
        $("#m2").show();
        $("#color-II").spectrum({
          preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]],
            move:function(color){
              $('.profile-text > h2').css('background-color', color.toHexString());
              $('.para3').css('background-color', color.toHexString());
            }
        });
      });
      $("#btn-three").click(function() {
        $("#m3").show();
        $("#color-III").spectrum({
          preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]],
            move:function(color){
              $('.send').css('background-color', color.toHexString());
            }
        });
      });
      $("#btn-three").click(function() {
        $("#m4").show();
        $("#color-IV").spectrum({
          preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]],
            move:function(color){
              $('.send').css('color', color.toHexString());
            }
        });
      });
      $("#btn-three").click(function() {
        $("#m5").show();
        $("#color-V").spectrum({
          preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]],
            move:function(color){
              $('u').css('color', color.toHexString());
            }
        });
      });
});

// main-function //

$(document).ready(function(){
  $('.button-click').click(function() {
    var index = $(this).index();
    if($(this).hasClass('active')) {
      $(this).removeClass('active');
        $('.content-div').eq(index).addClass('buttonClicked')
    } else {
      $('.button-click').removeClass('active');
      $(this).addClass('active')
      $('.content-div').hide().eq(index).show().removeClass('buttonClicked')
    }
  });
  $('.content-div').addClass('addedContent');
});
